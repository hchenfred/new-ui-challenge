import React, {Component} from 'react';
import {ModalContainer, ModalDialog} from 'react-modal-dialog';

class ContactView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowingModal: false,
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleClick = () => this.setState({isShowingModal: true})
  handleClose = () => this.setState({isShowingModal: false})

  render() {
    return <button className="btn btn-primary contactButton" onClick={this.handleClick}>
      {
        this.state.isShowingModal &&
        <ModalContainer onClose={this.handleClose}>
          <ModalDialog onClose={this.handleClose}>
            <h2>{this.props.fullName}</h2>
            <p>Phone #: {this.props.candidate.phone}</p>
            <p>Cell #: {this.props.candidate.cell}</p>
            <a href={`mailto:${this.props.candidate.email}`}>Email: {this.props.candidate.email}</a>
          </ModalDialog>
        </ModalContainer>
      }
    Contact</button>;
  }
};

export default ContactView;