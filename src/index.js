import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import CandidateList from './CandidateList';
import FilterBar from './FilterBar';
import {calculateAge} from './utility';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      matchedResults: [],
      filteredResults: [],
      filterSelected: false
    }
    this.applyFilter = this.applyFilter.bind(this);
    this.applyReset = this.applyReset.bind(this);
  }

  applyReset() {
    this.setState({filterSelected: false});
  }

  applyFilter(gender, minAge, maxAge) {
    const tempArray = this.state.matchedResults.filter((candidate)=> {
      let age = calculateAge(candidate.dob);
      if (gender === 'any') {
        return age >= minAge && age <= maxAge; 
      } else {
        return age >= minAge && age <= maxAge && candidate.gender === gender; 
      }
    });
    this.setState({filteredResults: tempArray, filterSelected: true});
  }

  componentDidMount() {
   const limit = 10;
   fetch(`https://randomuser.me/api/?results=${limit}`)
    .then((response) => response.json())
    .then((responseJson) => {
      console.log(responseJson.results);
      this.setState({ matchedResults: responseJson.results });
    })
    .catch((error) => {
      console.error(error);
    });
  }

  render () {
    let candidateList = this.state.filterSelected ? (
      <CandidateList candidates={this.state.filteredResults} />
    ) : (<CandidateList candidates={this.state.matchedResults}/>);
    return (
    <div className="container text-center">
      <div>
      <FilterBar applyFilter={this.applyFilter} applyReset={this.applyReset}/>
      {candidateList}
      </div>
    </div>)
  }
}

ReactDOM.render(<App />, document.getElementById('root'));