import React from 'react';
import CandidateInfo from './CandidateInfo';

const CandidateList = (props) => (
  <div id="searchResults">
    <h2> { props.candidates.length } Candidates Found </h2>
    <ul className="listContainer">
      { props.candidates.map(candidate => <CandidateInfo key={candidate.email} candidate={candidate}/>)}
    </ul>
  </div>
)

export default CandidateList;