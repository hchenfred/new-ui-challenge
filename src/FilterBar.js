import React, {Component} from 'react';

class FilterBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedGender: "any",
      selectedMinAge: "18",
      selectedMaxAge: "90"
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleGenderInputChange = this.handleGenderInputChange.bind(this);
    this.handleAgeMinChange = this.handleAgeMinChange.bind(this);
    this.handleAgeMaxChange = this.handleAgeMaxChange.bind(this);
    this.handleReset = this.handleReset.bind(this);
  }

  handleReset() {
    this.props.applyReset();
  }

  handleSubmit(e) {
    e.preventDefault();
    this.props.applyFilter(this.state.selectedGender, this.state.selectedMinAge, this.state.selectedMaxAge);
  }

  handleGenderInputChange(e) {
    this.setState({selectedGender: e.target.dataset.gender});
  }

  handleAgeMinChange(e) {
    this.setState({selectedMinAge: e.target.value});
  } 

  handleAgeMaxChange(e) {
    this.setState({selectedMaxAge: e.target.value});
  }

  render() {
    return (
      <div id="userPreferences">
        <h2>Search Criteria</h2>
        <form className="form" onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label className="control-label">Age</label>
            <input type="text" name="ageMin" value={this.state.selectedMinAge} onChange={this.handleAgeMinChange} /> to
            <input type="text" name="ageMax" value={this.state.selectedMaxAge} 
            onChange={this.handleAgeMaxChange} />
          </div>

          <div className="form-group">
            <label className="control-label">Gender</label>
            <label className="radio-inline"><input type="radio" data-gender="any" onChange={this.handleGenderInputChange} name="gender" checked={this.state.selectedGender === 'any'} />Any</label>
            <label className="radio-inline"><input type="radio" data-gender="male" onChange={this.handleGenderInputChange} name="gender"
            checked={this.state.selectedGender === 'male'} />Male</label>
            <label className="radio-inline"><input type="radio" data-gender="female" onChange={this.handleGenderInputChange} name="gender" 
            checked={this.state.selectedGender === 'female'}/>Female</label>
          </div>

          <button type="button" onClick={this.handleReset} className="btn btn-default">Reset</button>
          <button type="submit"  className="btn btn-primary">Filter</button>
        </form>
      </div>
    );
  }
};

export default FilterBar;