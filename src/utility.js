export const calculateAge = (dob) => {
  const currYear = parseInt((new Date()).getFullYear(), 10);
  return currYear - parseInt(dob.substring(0, 5), 10);
};