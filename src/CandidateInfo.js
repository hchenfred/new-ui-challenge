import React from 'react';
import ContactView from './ContactView';
import {calculateAge} from './utility';

const CandiateInfo = (props) => {
  const getFullName = (firstName, lastName) => {
    return `${firstName.charAt(0).toUpperCase()}${firstName.slice(1)} ${lastName.charAt(0).toUpperCase()}${lastName.slice(1)}`;
  };

  const fullName = getFullName(props.candidate.name.first, props.candidate.name.last);
 
  return (
    <div className="card">
      <img className="photo" src={props.candidate.picture.large} alt="candidate"/>
      <span className="name lead">{fullName}</span>
      <span className="age lead">{calculateAge(props.candidate.dob)}</span>
      <ContactView candidate={props.candidate} fullName={fullName}/>
    </div>
  );
};

export default CandiateInfo;